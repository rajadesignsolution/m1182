<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/about-banner.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>reviews</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>



      <section class="our-banner review-banner pad-tb" style="background-image:url(./assets1/images/review-bg.png);">
   
         <div class="container">         
            <div class="row align-items-center flexRevert pad-top">
               <div class="col-md-4">
                  <div class="singleClientimg">
                     <figure>
                        <img src="assets1/images/client/client1.jpg" alt="">
                     </figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="singleClientdis">
                     <div class="m5-h">
                        <h5>client's name</h5>
                     </div>
                     <div class="p4 mt-18">
                        <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                     </div>
                     <div class="p2 mt-35">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                     </div>
                     <ul class="socialIconinner mt-20">
                        <li class="first">
                           <a href="javascript:;">
                              <span class="fa fa-facebook-f"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-twitter"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-instagram"></span>
                           </a>
                        </li>
                        <li class="last">
                           <a href="javascript:;">
                              <span class="fa fa-linkedin"></span>
                           </a>
                        </li>
                     </ul>
                  </div>                  
               </div>
            </div>     
            <div class="row align-items-center pad-top">
               <div class="col-md-1"></div>
               <div class="col-md-7">
                  <div class="singleClientdis">
                     <div class="m5-h">
                        <h5>client's name</h5>
                     </div>
                     <div class="p4 mt-18">
                        <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                     </div>
                     <div class="p2 mt-35">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                     </div>
                     <ul class="socialIconinner mt-20">
                        <li class="first">
                           <a href="javascript:;">
                              <span class="fa fa-facebook-f"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-twitter"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-instagram"></span>
                           </a>
                        </li>
                        <li class="last">
                           <a href="javascript:;">
                              <span class="fa fa-linkedin"></span>
                           </a>
                        </li>
                     </ul>
                  </div>                  
               </div>
               <div class="col-md-4">
                  <div class="singleClientimg">
                     <figure>
                        <img src="assets1/images/client/client1.jpg" alt="">
                     </figure>
                  </div>
               </div>
            </div>        
            <div class="row align-items-center flexRevert pad-top">
               <div class="col-md-4">
                  <div class="singleClientimg">
                     <figure>
                        <img src="assets1/images/client/client1.jpg" alt="">
                     </figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="singleClientdis">
                     <div class="m5-h">
                        <h5>client's name</h5>
                     </div>
                     <div class="p4 mt-18">
                        <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                     </div>
                     <div class="p2 mt-35">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                     </div>
                     <ul class="socialIconinner mt-20">
                        <li class="first">
                           <a href="javascript:;">
                              <span class="fa fa-facebook-f"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-twitter"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-instagram"></span>
                           </a>
                        </li>
                        <li class="last">
                           <a href="javascript:;">
                              <span class="fa fa-linkedin"></span>
                           </a>
                        </li>
                     </ul>
                  </div>                  
               </div>
            </div>     
            <div class="row align-items-center pad-top">
               <div class="col-md-1"></div>
               <div class="col-md-7">
                  <div class="singleClientdis">
                     <div class="m5-h">
                        <h5>client's name</h5>
                     </div>
                     <div class="p4 mt-18">
                        <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                     </div>
                     <div class="p2 mt-35">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                     </div>
                     <ul class="socialIconinner mt-20">
                        <li class="first">
                           <a href="javascript:;">
                              <span class="fa fa-facebook-f"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-twitter"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-instagram"></span>
                           </a>
                        </li>
                        <li class="last">
                           <a href="javascript:;">
                              <span class="fa fa-linkedin"></span>
                           </a>
                        </li>
                     </ul>
                  </div>                  
               </div>
               <div class="col-md-4">
                  <div class="singleClientimg">
                     <figure>
                        <img src="assets1/images/client/client1.jpg" alt="">
                     </figure>
                  </div>
               </div>
            </div>        
            <div class="row align-items-center flexRevert pad-top">
               <div class="col-md-4">
                  <div class="singleClientimg">
                     <figure>
                        <img src="assets1/images/client/client1.jpg" alt="">
                     </figure>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="singleClientdis">
                     <div class="m5-h">
                        <h5>client's name</h5>
                     </div>
                     <div class="p4 mt-18">
                        <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                     </div>
                     <div class="p2 mt-35">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                     </div>
                     <ul class="socialIconinner mt-20">
                        <li class="first">
                           <a href="javascript:;">
                              <span class="fa fa-facebook-f"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-twitter"></span>
                           </a>
                        </li>
                        <li>
                           <a href="javascript:;">
                              <span class="fa fa-instagram"></span>
                           </a>
                        </li>
                        <li class="last">
                           <a href="javascript:;">
                              <span class="fa fa-linkedin"></span>
                           </a>
                        </li>
                     </ul>
                  </div>                  
               </div>
            </div>  
            <div class="row mt-50">
               <div class="col-md-12 text-right">
                  <a href="#" class="seeMore">see more</a>
               </div>
            </div>
         </div>

      </section>
      
 
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>