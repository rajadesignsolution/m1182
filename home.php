<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/banner-home.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>home</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>



      <section class="about-banner padtb-190-120" style="background-image:url(./assets1/images/about-background.png); ">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="row">                        
                     <div class="col-md-6 mt-35 col-xs-12 wow fadeInLeft">
                        <div class="m2-h">
                           <h5>about us</h5>
                        </div>

                        <div class="mt-50 p1 padRt-100 pmb">
                           <p>
                           Founded in 2017, MSA Construction is a renowned Central Florida residential construction firm with two decades of experience and a 
                           range of loyal customers to vouch for. We have been bringing a passion for energy efficiency and sustainability to our work, building 
                           affordable and safe community homes in Central Florida for over 3 years. MSA Construction, with deep roots in Central Florida, has 
                           been the area's leading supplier of conscientious and high-quality construction.
                           </p>
                        </div>
                        <div class="mt-50">                           
                           <a href="about.php" class="btn btn-read">read more</a>
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-12 wow fadeInRight">
                        <div class="text-right">                              
                           <figure>
                              <img src="assets1/images/about.jpg" alt="">
                           </figure>
                           <figure>
                              <img class="aboutAbsolute" src="assets1/images/about-1.jpg" alt="">
                           </figure>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      
      <section class="who-banner pad-tb">
         <div class="container-fluid">
            <div class="row flexRevert">    
               <div class="col-md-6 wow fadeInLeft pad">
                  <div class="text-left">                              
                     <figure>
                        <img src="assets1/images/who-we.png" alt="">
                     </figure>
                  </div>
               </div>                    
               <div class="col-md-4 wow fadeInRight mt-140">
                  <div class="m2-h">
                     <h5>who we are</h5>
                  </div>

                  <div class="mt-50 p1 padRt-100 pmb">
                     <p>
                     MSA Construction specialises in home building projects and new growth. We think the idea of building a new home should be exciting, not 
                     frustrating. Our team is enlivened about helping to build a home you enjoy for your families. We specialise in designing homes with the 
                     values of quality, fairness and dignity in Central Florida. Our company is built on the core values of family, commitment, and efficiency. 
                     </p>
                  </div>
                  <div class="mt-50">                           
                     <a href="about.php" class="btn btn-read">read more</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      


      <?php include("includes/short-services.php"); ?>

      <section class="our-banner pad-tb" style="background-image:url(./assets1/images/our-work.png);">
         <div class="container">
            <div class="row">  
               <div class="col-md-12">
                  <div class="m2-h">
                     <h5>our services</h5>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Residential-Construction.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Residential Construction</h5>
                     </div>
                     <div class="p4">
                        <p> Our breadth of skills gives us the ability to provide different strategies for residential construction, to incorporate... <a href="residential-construction.php" class="inlineReadmore">read more</a></p>
                                              
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Flooring.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Flooring</h5>
                     </div>
                     <div class="p4">
                     <p>Our flooring specialists are at your disposal, whether you want to remodel your entire home or just want to upgrade one room.... <a href="flooring.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Kitchen-Remodel.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Kitchen Remodels</h5>
                     </div>
                     <div class="p4">
                     <p>The nucleus of the house is the kitchen. A kitchen style requires to fulfil the needs of each household member, from birthday... <a href="kitchen-remodels.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Bathroom-Remodel.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Bathroom Remodels</h5>
                     </div>
                     <div class="p4">
                     <p>Glamorous or not, the room where we all start and end every day is the toilet. It should be a spot of respite, one that in... <a href="bathroom-remodels.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div> 
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Painting.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Painting</h5>
                     </div>
                     <div class="p4">
                     <p>With our painters and decorators expertise & consistency, we have the best tools for painting services in Central Florida in... <a href="painting.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Roofing.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Roofing</h5>
                     </div>
                     <div class="p4">
                     <p>The roof is extremely important to your house or organisation because it is the first line of protection against the outside... <a href="roofing.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 text-right">
                  <a href="services.php" class="seeMore">see more</a>
               </div>
            </div>
         </div>
      </section>



      <section class="our-work pad-tb">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="m2-h">
                     <h5>our work</h5>
                  </div>
               </div>
               <div class="col-md-8">
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Residential construction</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Flooring</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Kitchen Remodels</a>
                        <a class="nav-item nav-link" id="nav-Bathroom-tab" data-toggle="tab" href="#nav-Bathroom" role="tab" aria-controls="Bathroom" aria-selected="false">Bathroom Remodels</a>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
         <div class="container-fluid mt-35">   
            <div class="row">                        
               <div class="col-md-12">                  
                  <div class="tab-content" id="nav-tabContent">
                     <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-Bathroom" role="tabpanel" aria-labelledby="nav-Bathroom-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <?php include("includes/client-reviews.php"); ?>

      
 
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>