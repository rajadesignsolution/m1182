<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/about-banner.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>about</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>



      <section class="about-banner padtb-190-120" style="background-image:url(./assets1/images/about-background.png); ">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="row">                        
                     <div class="col-md-6 mt-35 col-xs-12 wow fadeInLeft">
                        <div class="m2-h">
                           <h5>about us</h5>
                        </div>

                        <div class="mt-50 p1 padRt-100 pmb">
                           <p>
                           Founded in 2017, MSA Construction is a renowned Central Florida residential construction firm with two decades of experience 
                           and a range of loyal customers to vouch for. We have been bringing a passion for energy efficiency and sustainability to our work, 
                           building affordable and safe community homes in Central Florida for over 3 years. MSA Construction, with deep roots in Central Florida, 
                           has been the area's leading supplier of conscientious and high-quality construction.
                           <br>
                           MSA Construction specialises in custom building architecture and new development. After our formation, we have worked tirelessly to 
                           build a team that can satisfy the demands of our customers in the remodelling and residential building industries with the highest 
                           expectations.
                           </p>
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-12 wow fadeInRight">
                        <div class="text-right">                              
                           <figure>
                              <img src="assets1/images/about.jpg" alt="">
                           </figure>
                           <figure>
                              <img class="aboutAbsolute" src="assets1/images/about-1.jpg" alt="">
                           </figure>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      
      <section class="who-banner pad-tb">
         <div class="container-fluid">
            <div class="row flexRevert">    
               <div class="col-md-6 wow fadeInLeft pad">
                  <div class="text-left">                              
                     <figure>
                        <img src="assets1/images/who-we.png" alt="">
                     </figure>
                  </div>
               </div>                    
               <div class="col-md-4 wow fadeInRight mt-140">
                  <div class="m2-h">
                     <h5>who we are</h5>
                  </div>

                  <div class="mt-50 p1 padRt-100 pmb">
                     <p>
                     MSA Construction specialises in home building projects and new growth. We think the idea of building a new home should be exciting, 
                     not frustrating. Our team is enlivened about helping to build a home you enjoy for your families. We specialise in designing homes with 
                     the values of quality, fairness and dignity in Central Florida. Our company is built on the core values of family, commitment, and 
                     efficiency. MSA Construction consists of people with experience in manual construction management and design-building. With enthusiasm, 
                     we create ventures and are inspired by the hope of the impact it would bring on our society. 
                     </p>
                  </div>
               </div>
            </div>
            <div class="row pad-tb">  
               <div class="col-md-2"></div>                
               <div class="col-md-4 wow fadeInLeft mt-140">
                  <div class="m2-h">
                     <h5>What Do We Do</h5>
                  </div>

                  <div class="mt-50 p1 padRt-100 pmb">
                     <p>
                     MSA Construction works with clients belonging to the diverse culture in the USA to design, construct and maintain their capital projects. 
                     On every project, we aim to drive value for customers. We work hard to make sure that every project is completed to our client’s utmost 
                     satisfaction, and with as little disruption to your lives as possible. Our purpose is to successfully deliver the customer vision into 
                     reality using our years of experience and committed team members. We credit our achievements to years of hard work, dedication to our 
                     clients, and a deep understanding of their interests and goals; providing real value in creating tomorrow's skylines.
                     </p>
                  </div>
               </div>  
               <div class="col-md-6 wow fadeInRight pad">
                  <div class="text-left">                              
                     <figure>
                        <img src="assets1/images/what-do.jpg" style="width:100%;" alt="">
                     </figure>
                  </div>
               </div>    
            </div>
         </div>
      </section>
      

      <section class="text-center pad-btm-70">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <figure>
                     <img src="assets1/images/bb.jpg" alt="">
                  </figure>
               </div>
               <div class="col-md-6">
                  <figure>
                     <img src="assets1/images/bb.jpg" alt="">
                  </figure>
               </div>
            </div>
         </div>
      </section>
      <?php include("includes/short-services.php"); ?>


      <section class="our-banner teamBanner heightVH-100 padtb-100-0" style="background-image:url(./assets1/images/our-work.png);">
   
         <div class="container">         
            <div class="row align-items-center pad-top">
               <div class="col-md-12 padbtm-120">
                  <div class="m2-h">
                     <h5>our team</h5>
                  </div>
                  <div class="p1 mt-35">
                     <p>
                     On the job and in the office, our team of highly trained individuals maintains professional craftsmanship and technical skill. Our goal is 
                     to surpass expectations beginning from our first meeting or phone call. We think for the owners of our homes and feel ourselves part of their 
                     communities. We aspire for outstanding contact between MSA Construction, our customers and any individual who helps in creating the dream.
                     </p>
                  </div>
               </div>
               <div class="col-md-3">
                  <!-- <div class="singleMember text-center">
                     <figure>
                        <img src="assets1/images/team/member1.png" alt="">
                     </figure>
                     <div class="singleMemberDis">
                        <div class="p8">
                           <p>Ben jonson</p>
                        </div>
                        <div class="p6 mt-20">
                           <p>
                           Lorem ipsum dolor sit  elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                           </p>
                        </div>
                        <ul class="mt-20">
                           <li class="first">
                              <a href="javascript:;">
                                 <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-instagram"></span>
                              </a>
                           </li>
                           <li class="last">
                              <a href="javascript:;">
                                 <span class="fa fa-linkedin"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div> -->

               </div>
               <div class="col-md-3">
                  <div class="singleMember text-center">
                     <figure>
                        <img src="assets1/images/team/steves_pic.jpg" alt="">
                     </figure>
                     <div class="singleMemberDis">
                        <div class="p8">
                           <p>Steves</p>
                        </div>
                        <div class="p6 mt-20">
                           <p>
                           Lorem ipsum dolor sit  elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                           </p>
                        </div>
                        <ul class="mt-20">
                           <li class="first">
                              <a href="javascript:;">
                                 <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-instagram"></span>
                              </a>
                           </li>
                           <li class="last">
                              <a href="javascript:;">
                                 <span class="fa fa-linkedin"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>

               </div>
               <div class="col-md-3">
                  <div class="singleMember text-center">
                     <figure>
                        <img src="assets1/images/team/Mike.jpg" alt="">
                     </figure>
                     <div class="singleMemberDis">
                        <div class="p8">
                           <p>Mike</p>
                        </div>
                        <div class="p6 mt-20">
                           <p>
                           Lorem ipsum dolor sit  elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                           </p>
                        </div>
                        <ul class="mt-20">
                           <li class="first">
                              <a href="javascript:;">
                                 <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-instagram"></span>
                              </a>
                           </li>
                           <li class="last">
                              <a href="javascript:;">
                                 <span class="fa fa-linkedin"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>

               </div>
               <div class="col-md-3">
                  <!-- <div class="singleMember text-center">
                     <figure>
                        <img src="assets1/images/team/member4.png" alt="">
                     </figure>
                     <div class="singleMemberDis">
                        <div class="p8">
                           <p>Ben jonson</p>
                        </div>
                        <div class="p6 mt-20">
                           <p>
                           Lorem ipsum dolor sit  elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                           </p>
                        </div>
                        <ul class="mt-20">
                           <li class="first">
                              <a href="javascript:;">
                                 <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                           <li>
                              <a href="javascript:;">
                                 <span class="fa fa-instagram"></span>
                              </a>
                           </li>
                           <li class="last">
                              <a href="javascript:;">
                                 <span class="fa fa-linkedin"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div> -->

               </div>
            </div>
         </div>

      </section>
      
      <?php include("includes/client-reviews.php"); ?>
 
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>