<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/services-banner.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>Services</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>


    <section class="serv-sec pad-tb">
        <div class="container">
            <div class="row">
            <div class="col-md-9">
                <div class="ser-r m2-h p1">
                    <div class="ser-rimg">
                        <img class="w-100" src="assets1/images/services/pools.jpg">
                    </div>
                    <h5>Pools</h5>
                    <p>Lorem ipsum dolor sit amet Mental Health adipiscing elit. Etiam aliquet odio non porta laoreet. Vestibulum in dui euismod, molestie quam porta, sagittis arcu. Pellentesque vitae pulvinar urna.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam aliquet odio non porta laoreet. Vestibulum in dui euismod, molestie quam porta, sagittis arcu. Pellentesque vitae pulvinar urna, in dignissim nulla. Mauris iaculis, tortor sed pharetra varius.</p>
                    <h6>Our Features Services</h6>
                    <a href="javascript:;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor totam nobis, eum harum omnis?</a>
                    <a href="javascript:;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. In.</a>
                    <a href="javascript:;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ad iusto earum quas ducimus.</a>
                </div>
            </div>
                <div class="col-md-3">
                    <div class="swo-bx m2-h">
                        <h5>Services We Offer:</h5>
                        <ul class="swo-lst">
                            <li class="first"><a href="residential-construction.php">Residential Construction</a></li>
                            <li><a href="flooring.php">Flooring</a></li>
                            <li><a href="kitchen-remodels.php">Kitchen Remodels</a></li>
                            <li><a href="bathroom-remodels.php">Bathroom Remodels</a></li>
                            <li><a href="painting.php">Painting</a></li>
                            <li><a href="roofing.php">Roofing</a></li>
                            <li><a href="outdoor-kitchens.php">Outdoor Kitchens</a></li>
                            <li><a href="pools.php">Pools</a></li>
                            <li><a href="fencing.php">Fencing</a></li>
                            <li><a href="pavers.php">Pavers</a></li>
                            <li class="last"><a href="screen-enclosures.php">Screen Enclosures</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

  
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>