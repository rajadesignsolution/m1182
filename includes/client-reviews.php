

 
      <section class="our-banner pad-tb">
         <div class="container">
            <div class="row">  
               <div class="col-md-12">
                  <div class="m2-h">
                     <h5>client's reviews</h5>
                  </div>
               </div>



               <!--Carousel Wrapper-->
               <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                  <!--Indicators-->
                  <ol class="carousel-indicators">
                  <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                  <li data-target="#multi-item-example" data-slide-to="1"></li>
                  <li data-target="#multi-item-example" data-slide-to="2"></li>
                  </ol>
                  <!--/.Indicators-->

                     <!--Slides-->
                     <div class="carousel-inner" role="listbox">

                        <!--First slide-->
                        <div class="carousel-item active">

                           <div class="row">
                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                           </div>

                        </div>
                        <!--/.First slide-->

                        <!--Second slide-->
                        <div class="carousel-item">

                           <div class="row">
                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                        <!--/.Second slide-->

                        <!--Third slide-->
                        <div class="carousel-item">

                           <div class="row">
                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="card mb-2">
                                    <img class="card-img-top" src="assets1/images/team/member1.png" alt="">
                                    <div class="card-body">
                                       <h4 class="card-title">Name here</h4>
                                       <div class="p5 mt-5">
                                          <p>company name here</p>
                                       </div>
                                       <div class="p6 mt-20">
                                          <p>
                                             Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                        <!--/.Third slide-->

                     </div>
                     <!--/.Slides-->

               </div>
               <!--/.Carousel Wrapper-->
               
            </div>
         </div>
      </section>