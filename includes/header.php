<header class="header">
	<div class="main-header">
		<div class="container-fluid">
			<div class="menu-Bar">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="row align-items-center">
				<div class="col-md-4 text-center">
					<a href="./" class="logo">
						<img src="assets1/images/logo-ft.png" alt="logo">
					</a>
				</div>
			
				<div class="col-md-12 text-left">
					<div class="menuWrap">
						<div class="row customRow">
							<div class="col-md-6">
								<ul class="menu wow fadeInLeft">
									<li>
										<a href="home.php">
											<!-- <span class="subtitle">The Difference is Our </span> -->
											home
										</a>
									</li>
									<li>
										<a href="about.php">
											<!-- <span class="subtitle">The Difference is Our </span> -->
											about us
										</a>
									</li>
									<li>
										<a href="services.php">
											<!-- <span class="subtitle">The Difference is Our </span> -->
											Services
										</a>
									</li>  
									<li>
										<a href="work.php">
											<!-- <span class="subtitle">The Difference is Our </span> -->
											our work
										</a>
									</li>                            
								</ul>
							</div>
							<div class="col-md-6">
								<ul class="menuSmall wow fadeInRight">  
									<li><a href="reviews.php">reviews</a></li>  
									<li><a href="contact.php">Contact Us</a></li>  
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
