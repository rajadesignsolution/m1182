
      <section class="about-banner pad-tb services-banner" style="background-image:url(./assets1/images/banner/services.jpg); ">
         <div class="container">
            <div class="row">
               <div class="col-md-4 text-center z-index-3">
                     <figure>
                        <img src="assets1/images/icon/creative.png" alt="">
                     </figure>
                  <div class="singleService">
                     <div class="m4-h">
                        <h5>Creative Plan & Design</h5>
                     </div>
                     <div class="p3">
                        <p>
                           Our research facilities continue to grow with the rapid advancement of technology. We are not onlookers to the success of our industry, but we are the leaders of building and project growth, We are its creators. To meet the demands of our consumers, we are constantly moving our employees, our programmes and our business forward with today's most innovative new technology.
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 text-center z-index-3">
                     <figure>
                        <img src="assets1/images/icon/talent.png" alt="">
                     </figure>
                  <div class="singleService">
                     <div class="m4-h">
                        <h5>Talented People </h5>
                     </div>
                     <div class="p3">
                        <p>
                        Throughout the journey of MSA construction, the members of our team have been more than craftspeople. They are innovative thinkers who 
                        create elegant and sustainable living environments that are energy-efficient. Together, for years to come, we will create a tradition of 
                        quality.
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 text-center z-index-3">
                     <figure>
                        <img src="assets1/images/icon/modern.png" alt="">
                     </figure>
                  <div class="singleService">
                     <div class="m4-h">
                        <h5>Modern Design </h5>
                     </div>
                     <div class="p3">
                        <p>
                        One of our core values has been to create homes that are new, comfortable, sturdy, and energy and water-efficient since the beginning of our business. We use 
                        design approaches to help consumers choose items that follow these principles for their houses.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>