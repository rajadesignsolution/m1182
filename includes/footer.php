<footer>
   <div class="container">
      <div class="footerBorder">
         <div class="row">

         <div class="col-md-6 text-left">
            <a href="./">
               <figure>
                  <img src="assets1/images/logo-ft.png" alt="">
               </figure>
            </a>
         </div>
         <div class="col-md-6 text-right mt-18">
            <a href="#" class="ftNumber">+10 783 3674 356</a>
         </div>
         </div>

      </div>

   </div>
   <div class="container">
      <div class="row">
         <div class="col-md-3">  
            <div class="ftr-lt">
               <div class="m4-h">
                  <h5>about</h5>
               </div>
               <div class="p3">
                  <p>
                     5th flora, 700/D kings road, green <br>
                     lane New York-1782<br>
                     +10 367 826 2567<br>
                     contact@carpenter.com
                  </p>
               </div>
            </div>

         </div>
         <div class="col-md-3"> 
            <div class="ftr-md1">
               <div class="m4-h">
                  <h5>services</h5>
               </div>
               <ul class="pageLink">
                  <li><a href="residential-construction.php">Residential Construction</a></li>
                  <li><a href="flooring.php">Flooring</a></li>
                  <li><a href="kitchen-remodels.php">Kitchen Remodels</a></li>
                  <li><a href="bathroom-remodels.php">Bathroom Remodels</a></li>
                  <li><a href="painting.php">Painting</a></li>
                  <li><a href="roofing.php">Roofing</a></li>
                  <li><a href="outdoor-kitchens.php">Outdoor Kitchens</a></li>
                  <li><a href="pools.php">Pools</a></li>
                  <li><a href="fencing.php">Fencing</a></li>
                  <li><a href="pavers.php">Pavers</a></li>
                  <li><a href="screen-enclosures.php">Screen Enclosures</a></li>
               </ul>
            </div>
         </div>
         <div class="col-md-3"> 
            <div class="ftr-md2">
               <div class="m4-h">
                  <h5>quick link</h5>
               </div>
               <ul class="pageLink">
                  <li><a href="home.php">home</a></li>
							<li><a href="about.php">about us</a></li>
                  <li><a href="services.php">Services</a></li>
                  <li><a href="work.php">our work</a></li> 
                  <li><a href="reviews.php">reviews</a></li>   
                  <li><a href="contact.php">Contact Us</a></li>        
               </ul>

            </div>

         </div>
         <div class="col-md-3"> 
            <div class="ftr-rt">
               <div class="m4-h">
                  <h5>follow us</h5>
               </div>
               <ul class="socialIcon">
                  <li>
                     <span class="fa fa-twitter"></span>
                  </li>
                  <li>
                     <span class="fa fa-google-plus"></span>
                  </li>
                  <li>
                     <span class="fa fa-facebook-f"></span>
                  </li>
                  <li>
                     <span class="fa fa-instagram"></span>
                  </li>

               </ul>
            </div>

         </div>

      </div>

   </div>
   
</footer>
   <!-- <div class="copyright text-center">
      <div class="container">
      <div class="row">
         <div class="col-md-12">
               <p>Copyright ©  2020 Jay Colar All rights reserved.</p>
         </div>
      </div>
      </div>
   </div> -->
