<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/services-banner.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>Services</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>


      <section class="our-banner pad-tb" style="background-image:url(./assets1/images/our-work.png);">
         <div class="container">
            <div class="row">  
               <div class="col-md-12">
                  <div class="m2-h">
                     <h5>our services</h5>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Residential-Construction.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Residential Construction</h5>
                     </div>
                     <div class="p4">
                        <p> Our breadth of skills gives us the ability to provide different strategies for residential construction, to incorporate... <a href="residential-construction.php" class="inlineReadmore">read more</a></p>
                                              
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Flooring.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Flooring</h5>
                     </div>
                     <div class="p4">
                     <p>Our flooring specialists are at your disposal, whether you want to remodel your entire home or just want to upgrade one room.... <a href="flooring.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Kitchen-Remodel.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Kitchen Remodels</h5>
                     </div>
                     <div class="p4">
                     <p>The nucleus of the house is the kitchen. A kitchen style requires to fulfil the needs of each household member, from birthday... <a href="kitchen-remodels.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Bathroom-Remodel.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Bathroom Remodels</h5>
                     </div>
                     <div class="p4">
                     <p>Glamorous or not, the room where we all start and end every day is the toilet. It should be a spot of respite, one that in... <a href="bathroom-remodels.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div> 
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Painting.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Painting</h5>
                     </div>
                     <div class="p4">
                     <p>With our painters and decorators expertise & consistency, we have the best tools for painting services in Central Florida in... <a href="painting.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Roofing.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Roofing</h5>
                     </div>
                     <div class="p4">
                     <p>The roof is extremely important to your house or organisation because it is the first line of protection against the outside... <a href="roofing.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/outdoor-kitchen.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Outdoor kitchens</h5>
                     </div>
                     <div class="p4">
                        <p>In certain aspects, our outdoor kitchens are unlike anything else on the market. With a comprehensive line of.... <a href="outdoor-kitchens.php" class="inlineReadmore">read more</a> </p>
                     </div>
                  </div>
               </div> 
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/pools.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>pools</h5>
                     </div>
                     <div class="p4">
                        <p>MSA Construction's pool services give you many convenient and practical options to keep your swimming... <a href="pools.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Fencing.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Fencing</h5>
                     </div>
                     <div class="p4">
                        <p >Whether it's about your family's wellbeing, your privacy or just for holding the dog in MSA Construction offers 
                        our residential... <a href="fencing.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div> 
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/Pavers.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Pavers</h5>
                     </div>
                     <div class="p4">
                        <p>Interlocking Pavers add design, aesthetic and enhance the outstanding value of the property as well as 
                        longevity since it... <a href="pavers.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="single-service">
                     <figure>
                        <img src="assets1/images/services/screen.jpg" alt="">
                     </figure>
                     <div class="m3-h">
                        <h5>Screen Enclosures </h5>
                     </div>
                     <div class="p4">
                        <p>Although we do just about all, in terms of re-screening or full removal, you can surely rely on us for 
                        the installation... <a href="screen-enclosures.php" class="inlineReadmore">read more</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <?php include("includes/short-services.php"); ?>


      <section class="our-banner teamBanner heightVH-100 padtb-100-0" style="background-image:url(./assets1/images/our-work.png);">
   
         <div class="container">         
            <div class="row align-items-center pad-top">
               <div class="col-md-12 padbtm-40">
                  <div class="m2-h">
                     <h5>our client</h5>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">   
            <div class="row align-items-center">    
               <div class="col-md-1"></div>     
               <div class="col-md-2">
                  <figure>
                     <img src="assets1/images/client/client1.jpg" alt="">
                  </figure>
               </div>     
               <div class="col-md-2">
                  <figure>
                     <img src="assets1/images/client/client1.jpg" alt="">
                  </figure>
               </div>     
               <div class="col-md-2">
                  <figure>
                     <img src="assets1/images/client/client1.jpg" alt="">
                  </figure>
               </div>     
               <div class="col-md-2">
                  <figure>
                     <img src="assets1/images/client/client1.jpg" alt="">
                  </figure>
               </div>     
               <div class="col-md-2">
                  <figure>
                     <img src="assets1/images/client/client1.jpg" alt="">
                  </figure>
               </div>
            </div>
         </div>
         
         <div class="container pad-top-80">
            <div class="row">
               <div class="col-md-6">
                  <div class="m2-h padbtm-40">
                     <h5>Some FAQ</h5>
                  </div>
                  <div id="accordion">
                     <div class="card">
                        <div class="card-header" id="headingOne">
                           <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              <h5 class="mb-0">
                              Choose between rates or instant payment
                              </h5>
                           </button>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                           <div class="card-body">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
                           </div>
                        </div>
                     </div>
                     <div class="card">
                        <div class="card-header" id="headingTwo">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              <h5 class="mb-0">
                              Choose between rates or instant payment
                              </h5>
                           </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                           <div class="card-body">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                           </div>
                        </div>
                     </div>
                     <div class="card">
                        <div class="card-header" id="headingThree">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              <h5 class="mb-0">
                              Choose between rates or instant payment
                              </h5>
                           </button>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                           <div class="card-body">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="m2-h padbtm-40">
                     <h5>ask questions ?</h5>
                  </div>
                  <div class="contactForm">                     
                     <form action="#" method="POST">                        
                        <input name="Name" type="text" class="form-control" id="forName" aria-describedby="Name" placeholder="Name">
                        <input name="Email" type="email" class="form-control" id="forEmail" aria-describedby="Email" placeholder="Email">
                        <input name="Phone" type="number" class="form-control" id="forPhone" aria-describedby="Phone" placeholder="Phone">                        
                        <textarea name="Message" placeholder="Message"></textarea>
                        <div>                           
                           <input type="submit" name="submit" vlaue="submit" class="btn btn-submit">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>

      </section>
      

      <section class="pad-tb">

      </section>
    
   <?php
      // Start Submiting form for email
      if(isset($_POST["submit"])){

         $Name = $_POST["Name"];
         $Email = $_POST["Email"];
         $Phone = $_POST["Phone"];
         $Message = $_POST["Message"];


         
        // Send Email

        $to = "noman@designstallion.com";
        $from = $Email;
        $subject = 'New Form Reqest';
        $headers  = "From: " . strip_tags($from) . "\r\n";
        $headers .= "Reply-To: ". strip_tags($from) . "\r\n";

       // $headers .= "CC: susan@example.com\r\n";

        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $message ='<table border="1"><tr><th>Name:</th><td>'.$Name.'</td></tr><tr><th>Email:</th><td>'.$Email.'</td></tr><tr><th>Phone:</th><td>'.$Phone.'</td></tr><tr><th>Message:</th><td>'.$Message.'</td></tr></table>';
        mail($to, $subject, $message, $headers);

        mail($from,$subject2,$message2,$headers2); 

        // sends a copy of the message to the sender
        
        echo "<p class='thankYou' style='text-align: center;font-size: 40px;margin: 0 0 30px;color: #294073;font-family: 'Poppins';font-weight: 600;letter-spacing: 1px;'>Mail Sent. Thank you we will contact you shortly</p>";
      }
   ?>
 
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
      <script>
         $(document).ready(function(){
            var maxLength = 112;
            $(".show-read-more").each(function(){
               var myStr = $(this).text();
               if($.trim(myStr).length > maxLength){
                  var newStr = myStr.substring(0, maxLength);
                  var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                  $(this).empty().html(newStr);
                  $(this).append(' <a href="javascript:void(0);" class="read-more">... read more</a>');
                  $(this).append('<span class="more-text">' + removedStr + '</span>');
               }
            });
            $(".read-more").click(function(){
               $(this).siblings(".more-text").contents().unwrap();
               $(this).remove();
            });
         });
      </script>
   </body>
</html>