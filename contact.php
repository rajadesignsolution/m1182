<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/contact-banner.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>contact us</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>
      
      <section class="conatcForm pad-tb">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="m6-h">
                     <h5>
                        <span>                     
                           We Want to Hear From You
                        </span>
                     </h5>
                  </div>
               </div>
            </div>
            <div class="row mt-35">               
               <div class="col-md-6 text-center">
                  <div class="addressDis">
                     <span class="fa fa-map-marker"></span>
                     <div class="p7">
                        <p>5th flora, 700/D kings road, green lane New York-1782</p>                     
                     </div>
                  </div>
               </div>               
               <div class="col-md-6 text-center">
                  <div class="addressDis">
                     <span class="fa fa-phone"></span>
                     <div class="p7">
                        <p>+10 367 826 2567</p>                     
                     </div>
                  </div>
               </div>
            </div>
            <form action="#" method="POST">
               <div class="row mt-50 formDis">
                  <div class="col-md-6">
                     <input type="text" id="fname" name="Fname" placeholder="First Name">
                  </div>
                  <div class="col-md-6">
                     <input type="text" id="lname" name="Lname" placeholder="Last Name">
                  </div>
                  <div class="col-md-6">
                     <input type="phone" id="phoneNumber" name="phoneNumber" placeholder="Phone Number">
                  </div>
                  <div class="col-md-6">
                     <input type="email" id="emailAddress" name="emailAddress" placeholder="Email Address">
                  </div>
                  <div class="col-md-6">
                     <input type="text" id="companyName" name="companyName" placeholder="Company Name">
                  </div>
                  <div class="col-md-6">
                     <input type="text" id="Subject" name="Subject" placeholder="Subject">
                  </div>
                  <div class="col-md-12">
                     <textarea placeholder="Comments" name="Comments"></textarea>
                  </div>
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">                                             
                           <input class="btn btn-submit" type="submit" name="submit" value="submit">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </section>


      <?php include("includes/client-reviews.php"); ?>

   <?php 
      if(isset($_POST["submit"])){

         $Fname = $_POST["Fname"];
         $Lname = $_POST["Lname"];
         $phoneNumber = $_POST["phoneNumber"];
         $emailAddress = $_POST["emailAddress"];
         $companyName = $_POST["companyName"];
         $Subject = $_POST["Subject"];
         $Comments = $_POST["Comments"];



         // Send Email

         $to = "noman@designstallion.com";
         $from = $Email;
         $subject = 'New Form Reqest';
         $headers  = "From: " . strip_tags($from) . "\r\n";
         $headers .= "Reply-To: ". strip_tags($from) . "\r\n";

         // $headers .= "CC: susan@example.com\r\n";

         $headers .= "MIME-Version: 1.0\r\n";
         $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
         $message ='<table border="1"><tr><th>First Name:</th><td>'.$Fname.'</td></tr><tr><th>Last Name:</th><td>'.$Lname.'</td></tr><tr><th>Phone Number:</th><td>'.$phoneNumber.'</td></tr><tr><th>Email Address:</th><td>'.$emailAddress.'</td></tr><tr><th>Company Name:</th><td>'.$companyName.'</td></tr><tr><th>Subject:</th><td>'.$Subject.'</td></tr><tr><th>Comments:</th><td>'.$Comments.'</td></tr></table>';
         mail($to, $subject, $message, $headers);

         mail($from,$subject2,$message2,$headers2); 

         // sends a copy of the message to the sender

         echo "<p class='thankYou' style='text-align: center;font-size: 40px;margin: 0 0 30px;color: #294073;font-family: 'Poppins';font-weight: 600;letter-spacing: 1px;'>Mail Sent. Thank you we will contact you shortly</p>";
      }
   ?>
 
      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>