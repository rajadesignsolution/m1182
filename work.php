<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("includes/compatibility.php"); ?>
      <meta name="description" content="">
      <title>MSA Construction LLC</title>
      <?php include("includes/style.php"); ?>

   </head>
   <body>
      <?php include("includes/header.php"); ?>
      <div class="mainBanner" style="background-image:url(./assets1/images/banner/work.jpg); ">
         <!-- <video preload="auto" autoplay="true" muted="false" loop="true" controls="false" id="myVideo">
           <source src="assets1/images/triple-v.mp4" type="video/mp4">
         </video> -->

         <div class="container z-9">
           <div class="row align-items-center">
              <div class="col-md-12">
                  <div class="m1-h text-center wow fadeInLeft">
                     <h5>our work</h5>
                  </div>
              </div>
           </div>
         </div>
      </div>



      <section class="our-work pad-140-0">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="m2-h">
                     <h5>our work</h5>
                  </div>
               </div>
               <div class="col-md-8">
                  <nav>
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Residential construction</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Flooring</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Kitchen Remodels</a>
                        <a class="nav-item nav-link" id="nav-Bathroom-tab" data-toggle="tab" href="#nav-Bathroom" role="tab" aria-controls="Bathroom" aria-selected="false">Bathroom Remodels</a>
                        <a class="nav-item nav-link" id="nav-painting-tab" data-toggle="tab" href="#nav-painting" role="tab" aria-controls="painting" aria-selected="false">painting roofing</a>
                        <a class="nav-item nav-link" id="nav-kitchens-tab" data-toggle="tab" href="#nav-kitchens" role="tab" aria-controls="kitchens" aria-selected="false">outdoor kitchens</a>
                        <a class="nav-item nav-link" id="nav-pools-tab" data-toggle="tab" href="#nav-pools" role="tab" aria-controls="pools" aria-selected="false">pools</a>
                        <a class="nav-item nav-link" id="nav-fencing-tab" data-toggle="tab" href="#nav-fencing" role="tab" aria-controls="fencing" aria-selected="false">Fencing</a>
                        <a class="nav-item nav-link" id="nav-pavers-tab" data-toggle="tab" href="#nav-pavers" role="tab" aria-controls="pavers" aria-selected="false">Pavers</a>
                        <a class="nav-item nav-link" id="nav-enclosures-tab" data-toggle="tab" href="#nav-enclosures" role="tab" aria-controls="enclosures" aria-selected="false">screen Enclosures</a>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
         <div class="container-fluid mt-35">   
            <div class="row">                        
               <div class="col-md-12">                  
                  <div class="tab-content" id="nav-tabContent">
                     <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-Bathroom" role="tabpanel" aria-labelledby="nav-Bathroom-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>                     
                     <div class="tab-pane fade" id="nav-painting" role="tabpanel" aria-labelledby="nav-painting-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>                   
                     <div class="tab-pane fade" id="nav-kitchens" role="tabpanel" aria-labelledby="nav-kitchens-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>                
                     <div class="tab-pane fade" id="nav-pools" role="tabpanel" aria-labelledby="nav-pools-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>              
                     <div class="tab-pane fade" id="nav-fencing" role="tabpanel" aria-labelledby="nav-fencing-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>           
                     <div class="tab-pane fade" id="nav-pavers" role="tabpanel" aria-labelledby="nav-pavers-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/about.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>       
                     <div class="tab-pane fade" id="nav-enclosures" role="tabpanel" aria-labelledby="nav-enclosures-tab">
                        
                        <div class="row">
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work3.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work1.jpg" alt="">
                              </figure>
                           </div>
                           <div class="col-md-3 pad">
                              <figure>
                                 <img src="assets1/images/work2.jpg" alt="">
                              </figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <?php include("includes/footer.php"); ?>
      <?php include("includes/scripts.php"); ?>
   </body>
</html>