<html lang="en-US" class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<title>MSA Construction LLC</title>
	<link rel="canonical" href="index.html" />
	<link rel="stylesheet" href="assets/includes/css/a5ff7.css" media="all" />
	<link rel="stylesheet" href="assets/includes/css/7347d.css" media="all" />
	<script src="assets/includes/js/minify/392b9.js"></script>
   <link rel="icon" href="" sizes="32x32" />
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

	
</head>
<style>
   
.logoHeader{
  width: 400px !important;
  height: auto !important;
}

.stripe-subtitle {
  display: none !important;
}
</style>
<body class="home page-template page-template-template-home page-template-template-home-php page page-id-2">
<div style="z-index: 9999; position: fixed; width: 100%; border: 0px; padding: 5px; margin: 0px; background-color: transparent;  font-size: 12pt; font-family: sans-serif;" id="web2disk_nag" onclick="javascript: document.getElementById('web2disk_nag').style.display = 'none';"></div>
<div class="canvas-wrapper">
	<div class="barba-progress"></div>
	<header id="masthead" class="site-header" role="banner">
		<div class="wrapper">
			<div class="grid-x align-middle top-nav">
				<div class="cell small-6 medium-4">
					<div class="logo">
						<a class="icon-costello-logo" href="index.php" rel="">
							<img src="assets1/images/logo-ft.png" alt="logo">
						</a>
					</div>
				</div>
				<div class="cell small-6 medium-8 end">
					<nav class="main-navigation navigation left" role="navigation">
						<a data-takeover="nav_takeover" class="mobile-menu-icon no-barba logoHeader">
							
						</a>
					</nav>
					<nav class="main-navigation navigation right" role="navigation" style="display:none;">
						<a data-takeover="nav_takeover" class="mobile-menu-icon no-barba toggle-takeover">
							<div class="burg"></div>
						</a>
					</nav>
				</div>
			</div>
		</div>
		<div id="nav_takeover" class="takeover takeover-slidedown">
			<section class="takeover-content">
				<div class="grid-container height100">
					<div class="grid-x height100 align-center">
						<div class="small-12 xsmall-11 cell">
							<div class="grid-x grid-margin-x">
								<div class="auto cell nav-content">
									<ul class="menu-header no-bullet">
										<li class="menu-item">
											<a href="home.php">
												<span class="subtitle">We Craft </span>
												<span class="title">Home </span>
											</a>
										</li>
										<li class="menu-item">
											<a href="about.php">
												<span class="subtitle">The Difference is Our </span>
												<span class="title">about us</span>
											</a>
										</li>
										<li class="menu-item">
											<a href="services.php">
												<span class="subtitle">30% Self- </span>
												<span class="title">Services </span>
											</a>
										</li>
										<li class="menu-item">
											<a href="work.php">
												<span class="subtitle">30% Self- </span>
												<span class="title">Our work </span>
											</a>
										</li>
									</ul>
								</div>
								<div class="shrink cell nav-extras">
									<div class="logo">
										<a class="icon-costello-logo" href="index.php" rel="home">
											<span class="text-indent">Home </span>
										</a>
									</div>
									<ul class="side-menu-header no-bullet">
										<li class="menu-item">
											<a href="reviews.php">Reviews</a>
										</li>
										<li class="menu-item">
											<a href="contact.php">contact us</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</header>
	<section id="home_stripes">
		<div class="stripe-container">
			<div class="stripe-wrapper">
				<a href="home.php" class="show-for-xmedium stripe stripe-experience">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/banner-home.jpg" data-src="assets1/images/banner/banner-home.jpg"></div>
					<div class="stripe-bg-img hover b-lazy" src="assets1/images/banner/banner-home.jpg" data-src="assets1/images/banner/banner-home.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">
							<div class="stripe-subtitle">We Craft </div> Home 
						</div>
						<span class="plus"></span>
						<span class="go">
							<span>GO </span>
						</span>
					</div>
				</a>
				<a href="home.php" class="hide-for-xmedium stripe-mobile stripe-experience">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/banner-home.jpg" data-src="assets1/images/banner/banner-home.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">Home </div>
						<span class="plus"></span>
					</div>
				</a>
				<a href="about.php" class="show-for-xmedium stripe stripe-people">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/about-banner.jpg" data-src="assets1/images/banner/about-banner.jpg"></div>
					<div class="stripe-bg-img hover b-lazy" src="assets1/images/banner/about-banner.jpg" data-src="assets1/images/banner/about-banner.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">
							<div class="stripe-subtitle">The Difference is Our </div> about 
						</div>
						<span class="plus"></span>
						<span class="go">
							<span>GO </span>
						</span>
					</div>
            </a>
            
				<a href="about.php" class="hide-for-xmedium stripe-mobile stripe-people">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/about-banner.jpg" data-src="assets1/images/banner/about-banner.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">about </div>
						<span class="plus"></span>
					</div>
				</a>
				<a href="services.php" class="show-for-xmedium stripe stripe-building">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/services-banner.jpg" data-src="assets1/images/banner/services-banner.jpg"></div>
					<div class="stripe-bg-img hover b-lazy" src="assets1/images/banner/services-banner.jpg" data-src="assets1/images/banner/services-banner.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">
							<div class="stripe-subtitle">See What We've Been </div> services 
						</div>
						<span class="plus"></span>
						<span class="go">
							<span>GO </span>
						</span>
					</div>
				</a>
				<a href="services.php" class="hide-for-xmedium stripe-mobile stripe-building">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/services-banner.jpg" data-src="assets1/images/banner/services-banner.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">services </div>
						<span class="plus"></span>
					</div>
				</a>
				<a href="work.php" class="show-for-xmedium stripe stripe-performance">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/work.jpg" data-src="assets1/images/banner/work.jpg" ></div>
					<div class="stripe-bg-img hover b-lazy" src="assets1/images/banner/work.jpg" data-src="assets1/images/banner/work.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">
							<div class="stripe-subtitle">30% Self- </div> our work 
						</div>
						<span class="plus"></span>
						<span class="go">
							<span>GO </span>
						</span>
					</div>
				</a>
				<a href="work.php" class="hide-for-xmedium stripe-mobile stripe-performance">
					<div class="stripe-bg-img b-lazy" src="assets1/images/banner/work.jpg" data-src="assets1/images/banner/work.jpg"></div>
					<div class="stripe-content">
						<div class="stripe-title">our work </div>
						<span class="plus"></span>
					</div>
				</a>
			</div>
		</div>
	</section>
	<div id="barba-wrapper">
		<div class="barba-container " data-namespace="homepage" id="has-barba">
			<div id="body" data-classes="home page-template page-template-template-home page-template-template-home-php page page-id-2"></div>
			<div class="wrapper" id="content">
				<main role="main"></main>
			</div>
		</div>
	</div>
</div>

	<script src="assets/includes/js/minify/ca095.js"></script>
	<script src="assets/includes/js/minify/d28bf.js"></script>
	<script src="assets/includes/js/minify/66aa9.js"></script>
	<!--
-->
</body></html>